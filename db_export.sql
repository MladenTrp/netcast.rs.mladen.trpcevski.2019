-- MySQL dump 10.13  Distrib 5.7.24, for Win64 (x86_64)
--
-- Host: localhost    Database: teretana
-- ------------------------------------------------------
-- Server version	5.7.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dolazak`
--

DROP TABLE IF EXISTS `dolazak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dolazak` (
  `id_dolaska` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_operatera` int(10) unsigned NOT NULL,
  `id_korisnika` int(10) unsigned NOT NULL,
  `id_trenera` int(10) unsigned DEFAULT NULL,
  `dolazak` datetime NOT NULL,
  `odlazak` datetime DEFAULT NULL,
  `iznos` decimal(7,2) unsigned NOT NULL,
  PRIMARY KEY (`id_dolaska`),
  KEY `fk_dolasci_2_idx` (`id_trenera`),
  KEY `fk_operater_idx` (`id_operatera`),
  KEY `fk_dolasci_1_idx` (`id_korisnika`),
  CONSTRAINT `fk_dolasci_1` FOREIGN KEY (`id_korisnika`) REFERENCES `korisnik` (`id_korisnika`) ON UPDATE CASCADE,
  CONSTRAINT `fk_dolasci_2` FOREIGN KEY (`id_trenera`) REFERENCES `trener` (`id_trenera`) ON UPDATE CASCADE,
  CONSTRAINT `fk_operater` FOREIGN KEY (`id_operatera`) REFERENCES `operater` (`id_operatera`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dolazak`
--

LOCK TABLES `dolazak` WRITE;
/*!40000 ALTER TABLE `dolazak` DISABLE KEYS */;
INSERT INTO `dolazak` VALUES (1,1,1,NULL,'2019-03-10 12:30:18','2019-03-10 12:30:52',0.00),(2,1,3,NULL,'2019-03-10 12:31:47','2019-03-10 12:32:09',1000.00),(3,1,3,NULL,'2019-03-10 12:32:43','2019-03-10 12:33:27',1000.00),(4,1,3,2,'2019-03-10 12:33:44','2019-03-10 12:38:24',1838.32);
/*!40000 ALTER TABLE `dolazak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupa`
--

DROP TABLE IF EXISTS `grupa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupa` (
  `id_grupe` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `naziv` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `popust` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`id_grupe`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupa`
--

LOCK TABLES `grupa` WRITE;
/*!40000 ALTER TABLE `grupa` DISABLE KEYS */;
INSERT INTO `grupa` VALUES (3,'studenti',12),(4,'srednjoskolci',10);
/*!40000 ALTER TABLE `grupa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `korisnik`
--

DROP TABLE IF EXISTS `korisnik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `korisnik` (
  `id_korisnika` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ime` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `prezime` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `tippret` tinyint(1) unsigned NOT NULL,
  `id_grupe` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_korisnika`),
  KEY `fk_grupe_idx` (`id_grupe`),
  CONSTRAINT `fk_grupe` FOREIGN KEY (`id_grupe`) REFERENCES `grupa` (`id_grupe`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `korisnik`
--

LOCK TABLES `korisnik` WRITE;
/*!40000 ALTER TABLE `korisnik` DISABLE KEYS */;
INSERT INTO `korisnik` VALUES (1,'Petar','Petrovic','petar.petrovic@gmail.com',1,3),(2,'Milos','Milosevic','milos.milosevic@gmail.com',1,NULL),(3,'Djordje','Djordjevic','djordje.djordjevic@gmail.com',0,3),(4,'Stojan','Stojanovic','stojan.stojanovic@gmail.com',0,NULL);
/*!40000 ALTER TABLE `korisnik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operater`
--

DROP TABLE IF EXISTS `operater`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operater` (
  `id_operatera` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ime` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `prezime` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `lozinka` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_operatera`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operater`
--

LOCK TABLES `operater` WRITE;
/*!40000 ALTER TABLE `operater` DISABLE KEYS */;
INSERT INTO `operater` VALUES (1,'Lazar','Lazarevic','lazar.lazarevic@gmail.com','lazar');
/*!40000 ALTER TABLE `operater` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pretplata`
--

DROP TABLE IF EXISTS `pretplata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pretplata` (
  `id_pretplate` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_korisnika` int(10) unsigned NOT NULL,
  `pocetak` date NOT NULL,
  `kraj` date NOT NULL,
  PRIMARY KEY (`id_pretplate`),
  KEY `fk_pretplate_1_idx` (`id_korisnika`),
  CONSTRAINT `fk_pretplate_1` FOREIGN KEY (`id_korisnika`) REFERENCES `korisnik` (`id_korisnika`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pretplata`
--

LOCK TABLES `pretplata` WRITE;
/*!40000 ALTER TABLE `pretplata` DISABLE KEYS */;
INSERT INTO `pretplata` VALUES (1,1,'2019-02-26','2019-02-28'),(4,2,'2019-02-26','2019-02-28'),(7,1,'2019-03-03','2019-03-31'),(8,2,'2019-03-03','2019-03-31');
/*!40000 ALTER TABLE `pretplata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trener`
--

DROP TABLE IF EXISTS `trener`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trener` (
  `id_trenera` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ime` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `prezime` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `cena` decimal(7,2) unsigned NOT NULL,
  PRIMARY KEY (`id_trenera`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trener`
--

LOCK TABLES `trener` WRITE;
/*!40000 ALTER TABLE `trener` DISABLE KEYS */;
INSERT INTO `trener` VALUES (1,'Milos','Pavlovic',880.00),(2,'Marko','Petrovic',1089.00);
/*!40000 ALTER TABLE `trener` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-10 12:54:19
