package Mladen.Teretana.mail;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import Mladen.Teretana.dao.DolazakRepository;
import Mladen.Teretana.dao.KorisnikRepository;
import Mladen.Teretana.domain.Dolazak;
import Mladen.Teretana.domain.Korisnik;
import Mladen.Teretana.domain.dto.DolazakDto2;
import Mladen.Teretana.domain.dto.PorukaDto;

@Service
public class EmailService
{
	@Autowired
	public JavaMailSender emailSender;
	
	@Autowired
	public DolazakRepository dolazakRepository;
	
	@Autowired
	public KorisnikRepository korisnikRepository;
	
	@Scheduled(cron = "0 0 0 1 * ?")
	public void posaljiMesecniPregled()
	{
		LocalDateTime mesecUnazad = LocalDateTime.now().minusMonths(1);
		int godina = mesecUnazad.getYear();
		int mesec = mesecUnazad.getMonthValue();
		LocalDateTime pocetak = LocalDateTime.of(godina, mesec, 1, 0, 0);
		LocalDateTime kraj = LocalDateTime.now();
		List<Korisnik> listaK = korisnikRepository.findAll();
		if (listaK.isEmpty()) return;
		int b = listaK.size();
		List<PorukaDto> niz = new ArrayList<PorukaDto>();
		for (int i=0; i<b; i++)
		{
			Korisnik k = listaK.get(i);
			PorukaDto pd = new PorukaDto();
			pd.setIdKorisnika(k.getIdkorisnika());
			pd.setIme(k.getIme());
			pd.setPrezime(k.getPrezime());
			pd.setEmail(k.getEmail());
			if (k.getTippret()==0) pd.setPretplata(0);
			else pd.setPretplata(5000); // iznos kopiran iz DolazakService
			pd.setUkupanIznos(pd.getPretplata());
			List<DolazakDto2> ldd = new ArrayList<DolazakDto2>();
			pd.setDolasci(ldd);
			niz.add(pd);
		}
		List<Dolazak> listaD = dolazakRepository.findByDolazakBetweenOrderByKorisnik(pocetak, kraj);
		int idk = 0;
		int j = 0;
		if (!listaD.isEmpty())
		{
			idk = listaD.get(0).getKorisnik().getIdkorisnika();
			int m = 0;
			while (m<b)
			{
				if (niz.get(m).getIdKorisnika()==idk)
				{
					j=m;
					break;
				}
				m++;
			}
		}
		for (int i=0; i<listaD.size(); i++)
		{
			Dolazak d = listaD.get(i);
			Korisnik k = d.getKorisnik();
			if (k.getIdkorisnika()==idk)
			{
				DolazakDto2 dd = new DolazakDto2();
				dd.setDatum(d.getDolazak().toLocalDate());
				dd.setIznos(d.getIznos());
				niz.get(j).setUkupanIznos(niz.get(j).getUkupanIznos() + d.getIznos());
				niz.get(j).getDolasci().add(dd);
			}
			else
			{
				idk = k.getIdkorisnika();
				int m = 0;
				while (m<b)
				{
					if (niz.get(m).getIdKorisnika()==idk)
					{
						j=m;
						break;
					}
					m++;
				}
				DolazakDto2 dd = new DolazakDto2();
				dd.setDatum(d.getDolazak().toLocalDate());
				dd.setIznos(d.getIznos());
				niz.get(j).setUkupanIznos(niz.get(j).getUkupanIznos() + d.getIznos());
				niz.get(j).getDolasci().add(dd);
			}
		}
		for (int n=0; n<b; n++)
		{
			try
			{
				SimpleMailMessage poruka = new SimpleMailMessage();
				poruka.setTo(niz.get(n).getEmail());
				poruka.setSubject("Teretana - mesecni pregled");
				String tekst = "Postovana / Postovani,\n";
				tekst += "Saljemo Vam spisak iskoriscenih termina i troskova za prethodni mesec:\n";
				for (int p=0; p<niz.get(n).getDolasci().size(); p++)
				{
					String dan = niz.get(n).getDolasci().get(p).getDatum().toString();
					double izn = niz.get(n).getDolasci().get(p).getIznos();
					tekst += String.format("%s : %.2f din\n", dan, izn);
				}
				tekst += String.format("Iznos pretplate: %.2f\n", niz.get(n).getPretplata());
				tekst += String.format("Ukupan iznos: %.2f din\n", niz.get(n).getUkupanIznos());
				poruka.setText(tekst);
				emailSender.send(poruka);
			}
			catch (MailException ex)
			{
				System.err.println(ex.getMessage());
			}
		}
	}
}