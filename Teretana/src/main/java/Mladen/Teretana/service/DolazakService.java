package Mladen.Teretana.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Mladen.Teretana.dao.DolazakRepository;
import Mladen.Teretana.dao.KorisnikRepository;
import Mladen.Teretana.dao.OperaterRepository;
import Mladen.Teretana.dao.PretplataRepository;
import Mladen.Teretana.dao.TrenerRepository;
import Mladen.Teretana.domain.Dolazak;
import Mladen.Teretana.domain.Korisnik;
import Mladen.Teretana.domain.Operater;
import Mladen.Teretana.domain.Trener;
import Mladen.Teretana.domain.dto.DolazakDto;
import Mladen.Teretana.domain.dto.KorisnikDto;
import Mladen.Teretana.domain.dto.response.DodajDolazakResponseDto;
import Mladen.Teretana.domain.dto.response.DodajOdlazakResponseDto;
import Mladen.Teretana.domain.dto.response.DodajTreningResponseDto;
import Mladen.Teretana.domain.dto.response.MesecniRacunResponseDto;

@Service
public class DolazakService 
{
	double tarifaJed = 1000;
	double tarifaMes = 5000;
	
	@Autowired
	private DolazakRepository dolazakRepository;
	
	@Autowired
	private KorisnikRepository korisnikRepository;
	
	@Autowired
	private TrenerRepository trenerRepository;
	
	@Autowired
	private PretplataRepository pretplataRepository;
	
	@Autowired
	private OperaterRepository operaterRepository;
	
	public DodajDolazakResponseDto dodajDolazakS(Integer idk, String email)
	{
		Optional<Korisnik> optionalKorisnik = korisnikRepository.findById(idk);
		DodajDolazakResponseDto response = new DodajDolazakResponseDto();
		if (!optionalKorisnik.isPresent())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Ne postoji korisnik za uneti ID korisnika!");
			return response;
		}
		Korisnik korisnik = optionalKorisnik.get();
		List<Dolazak> listaDolazaka = dolazakRepository.findFirstByKorisnikOrderByDolazakDesc(korisnik);
		if (!listaDolazaka.isEmpty())
		{
			if (listaDolazaka.get(0).getOdlazak()==null)
			{
				response.setUspesnost(false);
				response.setErrorMessage("Korisnik jos nije otisao!");
				return response;
			}
		}
		Dolazak d = new Dolazak();
		LocalDateTime dolazak = LocalDateTime.now();
		double iznos = 0;
		if (korisnik.getTippret().equals((byte) 0))
		{
			iznos=tarifaJed;
			if (korisnik.getGrupa()!=null)
			{
				int popust = korisnik.getGrupa().getPopust();
				double mnozilac = (100 - popust) / 100.00;
				iznos = iznos * mnozilac;
			}
		}
		d.setKorisnik(korisnik);
		d.setDolazak(dolazak);
		d.setIznos(iznos);
		List<Operater> optionalOperater = operaterRepository.findByEmail(email);
		Operater operater = optionalOperater.get(0);
		d.setOperater(operater);
		dolazakRepository.save(d);
		response.setUspesnost(true);
		response.setOperater(operater);
		response.setKorisnik(korisnik);
		response.setVreme(dolazak);
		response.setIznos(iznos);
		return response;
	}
	
	public DodajTreningResponseDto dodajTreningS(Integer idk, Integer idt)
	{
		Optional<Korisnik> optionalKorisnik = korisnikRepository.findById(idk);
		DodajTreningResponseDto response = new DodajTreningResponseDto();
		if (!optionalKorisnik.isPresent())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Ne postoji korisnik za uneti ID korisnika!");
			return response;
		}
		Optional<Trener> optionalTrener = trenerRepository.findById(idt);
		if (!optionalTrener.isPresent())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Ne postoji trener za uneti ID trenera!");
			return response;
		}
		Korisnik korisnik = optionalKorisnik.get();
		List<Dolazak> lista = dolazakRepository.findFirstByKorisnikOrderByDolazakDesc(korisnik);
		if (lista.isEmpty())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Nemoguce, korisnik uopste nije dolazio!");
			return response;
		}
		Dolazak dolazak = lista.get(0);
		if (dolazak.getOdlazak()!=null)
		{
			response.setUspesnost(false);
			response.setErrorMessage("Nemoguce, korisnik je otisao!");
			return response;
		}
		if (dolazak.getTrener()!=null)
		{
			response.setUspesnost(false);
			response.setErrorMessage("Korisnik je vec jednom trenirao!");
			return response;
		}
		Trener trener = optionalTrener.get();
		dolazak.setTrener(trener);
		double iznos = dolazak.getIznos();
		double iznos2 = trener.getCena();
		if (korisnik.getGrupa()!=null)
		{
			int popust = korisnik.getGrupa().getPopust();
			double mnozilac = (100 - popust) / 100.00;
			iznos2 = iznos2 * mnozilac;
		}
		iznos += iznos2;
		dolazak.setIznos(iznos);
		dolazakRepository.save(dolazak);
		int brojTreninga = dolazakRepository.countByTrener(trener);
		if ((brojTreninga>0) && ((brojTreninga%10)==0))
		{
			double cena = trener.getCena();
			cena = cena * 1.1;
			trener.setCena(cena);
			trenerRepository.save(trener);
		}
		response.setUspesnost(true);
		response.setTrener(trener);
		response.setNoviIznos(iznos);
		return response;
	}
	
	public DodajOdlazakResponseDto dodajOdlazakS(Integer idk)
	{
		Optional<Korisnik> optionalKorisnik = korisnikRepository.findById(idk);
		DodajOdlazakResponseDto response = new DodajOdlazakResponseDto();
		if (!optionalKorisnik.isPresent())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Ne postoji korisnik za uneti ID korisnika!");
			return response;
		}
		Korisnik korisnik = optionalKorisnik.get();
		List<Dolazak> lista = dolazakRepository.findFirstByKorisnikOrderByDolazakDesc(korisnik);
		if (lista.isEmpty())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Nemoguce, korisnik uopste nije dolazio!");
			return response;
		}
		Dolazak dolazak = lista.get(0);
		if (dolazak.getOdlazak()!=null)
		{
			response.setUspesnost(false);
			response.setErrorMessage("Nemoguce, korisnik je otisao!");
			return response;
		}
		LocalDateTime odlazak = LocalDateTime.now();
		dolazak.setOdlazak(odlazak);
		dolazakRepository.save(dolazak);
		response.setUspesnost(true);
		response.setDolazak(dolazak);
		return response;
	}
	
	public List<Dolazak> listajSveDolaskeS()
	{
		return dolazakRepository.findAllByOrderByDolazakDesc();
	}
	
	public List<Dolazak> listajIzmedjuS(LocalDateTime vreme1, LocalDateTime vreme2)
	{
		return dolazakRepository.findByDolazakBetween(vreme1, vreme2);
	}
	
	public List<KorisnikDto> listajKorIzmedjuS(LocalDateTime vreme1, LocalDateTime vreme2)
	{
		List<Dolazak> ld = dolazakRepository.findByDolazakBetweenOrderByKorisnik(vreme1, vreme2);
		List<KorisnikDto> lkd = new ArrayList<KorisnikDto>();
		if (ld.isEmpty()) return lkd;
		Dolazak d = new Dolazak();
		d = ld.get(0);
		DolazakDto dd = new DolazakDto();
		dd.setDolazak(d.getDolazak());
		dd.setOdlazak(d.getOdlazak());
		if (d.getTrener()!=null) dd.setId_trenera(d.getTrener().getIdtrenera());
		dd.setIznos(d.getIznos());
		Korisnik k = new Korisnik();
		k = d.getKorisnik();
		KorisnikDto kd = new KorisnikDto();
		kd.setId_korisnika(k.getIdkorisnika());
		kd.setIme(k.getIme());
		kd.setPrezime(k.getPrezime());
		List<DolazakDto> ldd = new ArrayList<DolazakDto>();
		ldd.add(dd);
		kd.setDolasci(ldd);
		lkd.add(kd);
		int b = 0;
		for (int i=1; i<ld.size(); i++)
		{
			d = new Dolazak();
			d = ld.get(i);
			dd = new DolazakDto();
			dd.setDolazak(d.getDolazak());
			dd.setOdlazak(d.getOdlazak());
			if (d.getTrener()!=null) dd.setId_trenera(d.getTrener().getIdtrenera());
			dd.setIznos(d.getIznos());
			k = new Korisnik();
			k = d.getKorisnik();
			if (k.getIdkorisnika()!=lkd.get(b).getId_korisnika())
			{
				kd = new KorisnikDto();
				kd.setId_korisnika(k.getIdkorisnika());
				kd.setIme(k.getIme());
				kd.setPrezime(k.getPrezime());
				ldd = new ArrayList<DolazakDto>();
				ldd.add(dd);
				kd.setDolasci(ldd);
				lkd.add(kd);
				b++;
			}
			else lkd.get(b).getDolasci().add(dd);
		}
		return lkd;
	}
	
	public MesecniRacunResponseDto mesecniRacunS(Integer idk)
	{
		Optional<Korisnik> optionalKorisnik = korisnikRepository.findById(idk);
		MesecniRacunResponseDto response = new MesecniRacunResponseDto();
		if (!optionalKorisnik.isPresent())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Ne postoji korisnik za uneti ID korisnika!");
			return response;
		}
		Korisnik korisnik = optionalKorisnik.get();
		LocalDateTime sad = LocalDateTime.now();
		int godina = sad.getYear();
		int mesec = sad.getMonthValue();
		LocalDateTime prvi = LocalDateTime.of(godina, mesec, 1, 0, 0);
		List<Dolazak> lista = dolazakRepository.findByKorisnikAndDolazakBetween(korisnik, prvi, sad);
		List<DolazakDto> listaDto = new ArrayList<DolazakDto>();
		double ukupanIznos = 0;
		for (int i=0; i<lista.size(); i++)
		{
			Dolazak d = lista.get(i);
			DolazakDto dd = new DolazakDto();
			dd.setDolazak(d.getDolazak());
			if (d.getOdlazak()!=null) dd.setOdlazak(d.getOdlazak());
			if (d.getTrener()!=null) dd.setId_trenera(d.getTrener().getIdtrenera());
			dd.setIznos(d.getIznos());
			listaDto.add(dd);
			ukupanIznos += d.getIznos();
		}
		response.setUspesnost(true);
		response.setIme(korisnik.getIme());
		response.setPrezime(korisnik.getPrezime());
		response.setDolasci(listaDto);
		if (korisnik.getTippret()==1) ukupanIznos += tarifaMes;
		response.setUkupanIznos(ukupanIznos);
		return response;
	}
	
	public String prihodiSviS()
	{
		double p = 0;
		List<Dolazak> l = dolazakRepository.findAll();
		for (int i=0; i<l.size(); i++) p+=l.get(i).getIznos();
		p+=pretplataRepository.count()*tarifaMes;
		return String.format("Ukupni prihodi iznose %.2f din.", p);
	}
}