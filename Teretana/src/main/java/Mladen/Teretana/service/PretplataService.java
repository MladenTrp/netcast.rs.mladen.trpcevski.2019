package Mladen.Teretana.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Mladen.Teretana.dao.KorisnikRepository;
import Mladen.Teretana.dao.PretplataRepository;
import Mladen.Teretana.domain.Korisnik;
import Mladen.Teretana.domain.Pretplata;
import Mladen.Teretana.domain.dto.PretplataDto;
import Mladen.Teretana.domain.dto.response.ObrisiPretplatuResponseDto;
import Mladen.Teretana.domain.dto.response.PretplatiResponseDto;

@Service
public class PretplataService 
{
	@Autowired
	private PretplataRepository pretplataRepository;
	
	@Autowired
	private KorisnikRepository korisnikRepository;
	
	public PretplatiResponseDto pretplatiS(Integer idk) 
	{
		Optional<Korisnik> optionalKorisnik = korisnikRepository.findById(idk);
		PretplatiResponseDto response = new PretplatiResponseDto();
		if (!optionalKorisnik.isPresent())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Ne postoji korisnik za uneti ID korisnika!");
			return response;
		}
		Korisnik korisnik = optionalKorisnik.get();
		if (korisnik.getTippret().equals((byte) 1))
		{
			response.setUspesnost(false);
			response.setErrorMessage("Korisnik vec ima pretplatu!");
			return response;
		}
		Pretplata pretplata = new Pretplata();
		LocalDate pocetak = LocalDate.now();
		boolean prestupna = LocalDate.now().isLeapYear();
		int godina = LocalDate.now().getYear();
		int mesec = LocalDate.now().getMonthValue();
		int dan = LocalDate.now().getMonth().length(prestupna);
		LocalDate kraj = LocalDate.of(godina, mesec, dan);
		pretplata.setKorisnik(korisnik);
		pretplata.setPocetak(pocetak);
		pretplata.setKraj(kraj);
		pretplataRepository.save(pretplata);
		korisnik.setTippret((byte) 1);
		korisnikRepository.save(korisnik);
		response.setUspesnost(true);
		response.setKorisnik(korisnik);
		response.setPocetak(pocetak);
		response.setKraj(kraj);
		return response;
	}
	
	public List<Pretplata> listajSveS()
	{
		return pretplataRepository.findAllByOrderByKrajDesc();
	}
	
	public List<PretplataDto> azurirajPretplateS()
	{
		LocalDate datum = LocalDate.now().minusMonths(1);
		boolean prestupna = datum.isLeapYear();
		int dan1 = datum.getDayOfMonth();
		int dan2 = datum.getMonth().length(prestupna);
		int razlika = dan2 - dan1;
		LocalDate poslednji = datum.plusDays(razlika);
		List<Pretplata> lista = pretplataRepository.findByKraj(poslednji);
		List<PretplataDto> listaDto = new ArrayList<PretplataDto>();
		for (int i=0; i<lista.size(); i++)
		{
			Pretplata p = lista.get(i);
			PretplataDto dto = new PretplataDto();
			Korisnik k = p.getKorisnik();
			k.setTippret((byte) 0);
			korisnikRepository.save(k);
			dto.setIme(k.getIme());
			dto.setPrezime(k.getPrezime());
			dto.setPocetak(p.getPocetak());
			dto.setKraj(p.getKraj());
			listaDto.add(dto);
		}
		return listaDto;
	}
	
	public ObrisiPretplatuResponseDto obrisiPretplatuS(Integer idk)
	{
		Optional<Korisnik> optionalKorisnik = korisnikRepository.findById(idk);
		ObrisiPretplatuResponseDto response = new ObrisiPretplatuResponseDto();
		if (!optionalKorisnik.isPresent())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Ne postoji korisnik za uneti ID korisnika!");
			return response;
		}
		Korisnik korisnik = optionalKorisnik.get();
		if (korisnik.getTippret().equals((byte) 0))
		{
			response.setUspesnost(false);
			response.setErrorMessage("Korisnik nema tekucu pretplatu!");
			return response;
		}
		List<Pretplata> listaPretplata = pretplataRepository.findFirstByKorisnikOrderByKrajDesc(korisnik);
		if (listaPretplata.isEmpty())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Korisnik nema nijednu pretplatu u arhivi, ali status pretplate je korigovan.");
			korisnik.setTippret((byte) 0);
			korisnikRepository.save(korisnik);
			return response;
		}
		Pretplata poslednja = listaPretplata.get(0);
		if (poslednja.getKraj().isBefore(LocalDate.now()))
		{
			response.setUspesnost(false);
			response.setErrorMessage("Pretplata je vec istekla, ne sme se brisati iz arhive! Status pretplate je korigovan.");
			korisnik.setTippret((byte) 0);
			korisnikRepository.save(korisnik);
			return response;
		}
		int idp = poslednja.getIdpretplate();
		pretplataRepository.deleteById(idp);
		korisnik.setTippret((byte) 0);
		korisnikRepository.save(korisnik);
		response.setUspesnost(true);
		response.setPretplata(poslednja);
		return response;
	}
}