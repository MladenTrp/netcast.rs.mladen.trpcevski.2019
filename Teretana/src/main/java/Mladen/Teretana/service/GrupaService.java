package Mladen.Teretana.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Mladen.Teretana.dao.GrupaRepository;
import Mladen.Teretana.dao.KorisnikRepository;
import Mladen.Teretana.domain.Grupa;
import Mladen.Teretana.domain.Korisnik;
import Mladen.Teretana.domain.dto.response.DodajGrupuResponseDto;
import Mladen.Teretana.domain.dto.response.DodeliGrupiResponseDto;
import Mladen.Teretana.domain.dto.response.IzbaciIzGrupeResponseDto;
import Mladen.Teretana.domain.dto.response.ObrisiGrupuResponseDto;
import Mladen.Teretana.domain.dto.response.PromeniPopustResponseDto;

@Service
public class GrupaService
{
	@Autowired
	private GrupaRepository grupaRepository;
	
	@Autowired
	private KorisnikRepository korisnikRepository;
	
	public DodajGrupuResponseDto dodajGrupuS(String naziv, Byte popust)
	{
		DodajGrupuResponseDto response = new DodajGrupuResponseDto();
		if (popust <= 0)
		{
			response.setUspesnost(false);
			response.setErrorMessage("Popust mora biti pozitivan!");
			return response;
		}
		if (popust > 99)
		{
			response.setUspesnost(false);
			response.setErrorMessage("Popust ne sme biti veci od 99%!");
			return response;
		}
		if (naziv.equals(""))
		{
			response.setUspesnost(false);
			response.setErrorMessage("Morate uneti ime grupe!");
			return response;
		}
		List<Grupa> lista = grupaRepository.findByNaziv(naziv);
		if (!lista.isEmpty())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Vec postoji grupa sa unetim imenom!");
			return response;
		}
		Grupa grupa = new Grupa();
		grupa.setNaziv(naziv);
		grupa.setPopust(popust);
		grupaRepository.save(grupa);
		response.setUspesnost(true);
		response.setGrupa(grupa);
		return response;
	}
	
	public List<Grupa> listajGrupeS()
	{
		return grupaRepository.findAll();
	}
	
	public PromeniPopustResponseDto promeniPopustS(Integer id, Byte popust)
	{
		PromeniPopustResponseDto response = new PromeniPopustResponseDto();
		if (popust <= 0)
		{
			response.setUspesnost(false);
			response.setErrorMessage("Popust mora biti pozitivan!");
			return response;
		}
		if (popust > 99)
		{
			response.setUspesnost(false);
			response.setErrorMessage("Popust ne sme biti veci od 99%!");
			return response;
		}
		Optional<Grupa> optionalGrupa = grupaRepository.findById(id);
		if (!optionalGrupa.isPresent())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Ne postoji grupa za uneti ID grupe!");
			return response;
		}
		Grupa grupa = optionalGrupa.get();
		response.setStariPopust(grupa.getPopust());
		grupa.setPopust(popust);
		grupaRepository.save(grupa);
		response.setUspesnost(true);
		response.setNoviPopust(popust);
		return response;
	}
	
	public ObrisiGrupuResponseDto obrisiGrupuS(Integer id)
	{
		ObrisiGrupuResponseDto response = new ObrisiGrupuResponseDto();
		Optional<Grupa> optionalGrupa = grupaRepository.findById(id);
		if (!optionalGrupa.isPresent())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Ne postoji grupa za uneti ID grupe!");
			return response;
		}
		grupaRepository.deleteById(id);
		response.setUspesnost(true);
		return response;
	}
	
	public DodeliGrupiResponseDto dodeliGrupiS(Integer idg, Integer idk)
	{
		DodeliGrupiResponseDto response = new DodeliGrupiResponseDto();
		Optional<Grupa> optionalGrupa = grupaRepository.findById(idg);
		if (!optionalGrupa.isPresent())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Ne postoji grupa za uneti ID grupe!");
			return response;
		}
		Optional<Korisnik> optionalKorisnik = korisnikRepository.findById(idk);
		if (!optionalKorisnik.isPresent())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Ne postoji korisnik za uneti ID korisnika!");
			return response;
		}
		Korisnik korisnik = optionalKorisnik.get();
		if (korisnik.getGrupa()!=null)
		{
			response.setUspesnost(false);
			response.setErrorMessage("Korisnik vec pripada nekoj grupi!");
			return response;
		}
		Grupa grupa = optionalGrupa.get();
		korisnik.setGrupa(grupa);
		korisnikRepository.save(korisnik);
		response.setUspesnost(true);
		response.setKorisnik(korisnik);
		return response;
	}
	
	public IzbaciIzGrupeResponseDto izbaciIzGrupeS(Integer idg, Integer idk)
	{
		IzbaciIzGrupeResponseDto response = new IzbaciIzGrupeResponseDto();
		Optional<Grupa> optionalGrupa = grupaRepository.findById(idg);
		if (!optionalGrupa.isPresent())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Ne postoji grupa za uneti ID grupe!");
			return response;
		}
		Optional<Korisnik> optionalKorisnik = korisnikRepository.findById(idk);
		if (!optionalKorisnik.isPresent())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Ne postoji korisnik za uneti ID korisnika!");
			return response;
		}
		Korisnik korisnik = optionalKorisnik.get();
		Grupa grupa = optionalGrupa.get();
		if (korisnik.getGrupa()!=grupa)
		{
			response.setUspesnost(false);
			response.setErrorMessage("Korisnik nije clan ove grupe!");
			return response;
		}
		korisnik.setGrupa(null);
		korisnikRepository.save(korisnik);
		response.setUspesnost(true);
		return response;
	}
}