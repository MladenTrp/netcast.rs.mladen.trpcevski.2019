package Mladen.Teretana.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Mladen.Teretana.dao.TrenerRepository;
import Mladen.Teretana.domain.Dolazak;
import Mladen.Teretana.domain.Trener;
import Mladen.Teretana.domain.dto.TrenerDto;
import Mladen.Teretana.domain.dto.VezbanjeDto;
import Mladen.Teretana.domain.dto.response.DodajTreneraResponseDto;
import Mladen.Teretana.domain.dto.response.NadjiIdTreneraResponseDto;

@Service
public class TrenerService 
{
	@Autowired
	private TrenerRepository trenerRepository;
	
	public DodajTreneraResponseDto dodajTreneraS(String ime, String prezime, Double cena)
	{
		DodajTreneraResponseDto response = new DodajTreneraResponseDto();
		Trener trener = new Trener();
		trener.setIme(ime);
		trener.setPrezime(prezime);
		trener.setCena(cena);
		trenerRepository.save(trener);
		response.setIme(ime);
		response.setPrezime(prezime);
		response.setCena(cena);
		return response;
	}
	
	public List<Trener> listajTrenereS()
	{
		return trenerRepository.findAll();
	}
	
	public List<TrenerDto> listajTreningeS()
	{
		List<Trener> lista = trenerRepository.findAll();
		List<TrenerDto> listaDto = new ArrayList<TrenerDto>();
		for (int i=0; i<lista.size(); i++)
		{
			Trener t = lista.get(i);
			if (!t.getDolasci().isEmpty())
			{
				TrenerDto td = new TrenerDto();
				td.setId_trenera(t.getIdtrenera());
				td.setIme(t.getIme());
				td.setPrezime(t.getPrezime());
				td.setCena(t.getCena());
				List<VezbanjeDto> lvd = new ArrayList<VezbanjeDto>();
				List<Dolazak> ld = new ArrayList<Dolazak>();
				ld = t.getDolasci();
				for (int j=0; j<ld.size(); j++)
				{
					VezbanjeDto vd = new VezbanjeDto();
					Dolazak d = ld.get(j);
					vd.setImeKorisnika(d.getKorisnik().getIme());
					vd.setPrezimeKorisnika(d.getKorisnik().getPrezime());
					vd.setDatum(d.getDolazak().toLocalDate());
					lvd.add(vd);
				}
				td.setTreninzi(lvd);
				listaDto.add(td);
			}
		}
		return listaDto;
	}
	
	public NadjiIdTreneraResponseDto nadjiIdTreneraS(Integer id)
	{
		Optional<Trener> optionalTrener = trenerRepository.findById(id);
		NadjiIdTreneraResponseDto response = new NadjiIdTreneraResponseDto();
		if (!optionalTrener.isPresent())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Ne postoji trener za uneti ID trenera!");
			return response;
		}
		Trener trener = optionalTrener.get();
		response.setUspesnost(true);
		TrenerDto trenerDto = new TrenerDto();
		trenerDto.setId_trenera(trener.getIdtrenera());
		trenerDto.setIme(trener.getIme());
		trenerDto.setPrezime(trener.getPrezime());
		trenerDto.setCena(trener.getCena());
		List<VezbanjeDto> lvd = new ArrayList<VezbanjeDto>();
		List<Dolazak> ld = trener.getDolasci();
		for (int i=0; i<ld.size(); i++)
		{
			Dolazak d = ld.get(i);
			VezbanjeDto vd = new VezbanjeDto();
			vd.setImeKorisnika(d.getKorisnik().getIme());
			vd.setPrezimeKorisnika(d.getKorisnik().getPrezime());
			vd.setDatum(d.getDolazak().toLocalDate());
			lvd.add(vd);
		}
		trenerDto.setTreninzi(lvd);
		response.setTrener(trenerDto);
		return response;
	}
}