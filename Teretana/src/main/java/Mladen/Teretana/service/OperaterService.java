package Mladen.Teretana.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Mladen.Teretana.dao.OperaterRepository;
import Mladen.Teretana.domain.Dolazak;
import Mladen.Teretana.domain.Operater;
import Mladen.Teretana.domain.dto.OperaterDto;
import Mladen.Teretana.domain.dto.TerminDto;
import Mladen.Teretana.domain.dto.request.LoginRequestDto;
import Mladen.Teretana.domain.dto.request.OperaterRequestDto;
import Mladen.Teretana.domain.dto.response.LoginResponseDto;
import Mladen.Teretana.domain.dto.response.RegistrujOperateraResponseDto;
import Mladen.Teretana.security.TokenService;

@Service
public class OperaterService
{
	@Autowired
	private OperaterRepository operaterRepository;
	
	@Autowired
	private TokenService tokenServis;
	
	public RegistrujOperateraResponseDto registrujOperateraS(OperaterRequestDto operaterDto)
	{
		String email = operaterDto.getEmail();
		RegistrujOperateraResponseDto response = new RegistrujOperateraResponseDto();
		List<Operater> lista = operaterRepository.findByEmail(email);
		if (!lista.isEmpty())
		{
			response.setUspesnost(false);
			response.setErrorMessage("vec postoji korisnik sa unetom e-mail adresom!");
			return response;
		}
		Operater operater = new Operater();
		operater.setIme(operaterDto.getIme());
		operater.setPrezime(operaterDto.getPrezime());
		operater.setEmail(operaterDto.getEmail());
		operater.setLozinka(operaterDto.getLozinka());
		operaterRepository.save(operater);
		response.setUspesnost(true);
		return response;
	}
	
	public LoginResponseDto loginS(LoginRequestDto loginDto)
	{
		String email = loginDto.getEmail();
		String lozinka = loginDto.getLozinka();
		LoginResponseDto response = new LoginResponseDto();
		List<Operater> lista = operaterRepository.findByEmail(email); // Moze i findByEmailAndLozinka
		if (lista.isEmpty())
		{
			response.setUspesnost(false);
			response.setErrorMessage("Ne postoji korisnik sa unetom e-mail adresom!");
			return response;
		}
		Operater operater = lista.get(0);
		if (!operater.getLozinka().equals(lozinka))
		{
			response.setUspesnost(false);
			response.setErrorMessage("Pogresna lozinka!");
			return response;
		}
		response.setUspesnost(true);
		response.setToken(tokenServis.napraviToken(email));
		return response;
	}
	
	public List<OperaterDto> listajOperatereS()
	{
		List<Operater> lista = operaterRepository.findAll();
		List<OperaterDto> listaDto = new ArrayList<OperaterDto>();
		for (int i=0; i<lista.size(); i++)
		{
			Operater o = lista.get(i);
			OperaterDto od = new OperaterDto();
			od.setIme(o.getIme());
			od.setPrezime(o.getPrezime());
			od.setEmail(o.getEmail());
			List<Dolazak> ld = o.getDolasci();
			List<TerminDto> ltd = new ArrayList<TerminDto>();
			for (int j=0; j<ld.size(); j++)
			{
				Dolazak d = ld.get(j);
				TerminDto td = new TerminDto();
				td.setImeKorisnika(d.getKorisnik().getIme());
				td.setPrezimeKorisnika(d.getKorisnik().getPrezime());
				if (d.getTrener()!=null) td.setIdTrenera(d.getTrener().getIdtrenera());
				td.setDolazak(d.getDolazak());
				td.setOdlazak(d.getOdlazak());
				td.setIznos(d.getIznos());
				ltd.add(td);
			}
			od.setTermini(ltd);
			listaDto.add(od);
		}
		return listaDto;
	}
}