package Mladen.Teretana.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Trener
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_trenera")
	private Integer idtrenera;
	
	@JsonBackReference
	@OneToMany(mappedBy="trener")
	private List<Dolazak> dolasci;
	
	@Column(nullable=false)
	private String ime;
	
	@Column(nullable=false)
	private String prezime;
	
	@Column(nullable=false)
	private Double cena;
	
	public Integer getIdtrenera() {return idtrenera;}
	
	public List<Dolazak> getDolasci() {return dolasci;}
	public void setDolasci(List<Dolazak> dolasci) {this.dolasci = dolasci;}
	
	public String getIme() {return ime;}
	public void setIme(String ime) {this.ime = ime;}
	
	public String getPrezime() {return prezime;}
	public void setPrezime(String prezime) {this.prezime = prezime;}
	
	public Double getCena() {return cena;}
	public void setCena(Double cena) {this.cena = cena;}
}