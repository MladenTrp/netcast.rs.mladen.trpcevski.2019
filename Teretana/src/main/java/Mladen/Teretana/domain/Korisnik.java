package Mladen.Teretana.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Korisnik
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_korisnika")
	private Integer idkorisnika;
	
	@JsonBackReference
	@OneToMany(mappedBy="korisnik")
	private List<Pretplata> pretplate;
	
	@JsonBackReference
	@OneToMany(mappedBy="korisnik")
	private List<Dolazak> dolasci;
	
	@ManyToOne
	@JoinColumn(name="id_grupe")
	private Grupa grupa;
	
	@Column(nullable=false)
	private String ime;
	
	@Column(nullable=false)
	private String prezime;
	
	@Column(nullable=false)
	private String email;
	
	@Column(nullable=false)
	private Byte tippret;
	
	public Integer getIdkorisnika() {return idkorisnika;}
		
	public List<Pretplata> getPretplate() {return pretplate;}
	public void setPretplate(List<Pretplata> pretplate) {this.pretplate = pretplate;}
	
	public List<Dolazak> getDolasci() {return dolasci;}
	public void setDolasci(List<Dolazak> dolasci) {this.dolasci = dolasci;}
	
	public Grupa getGrupa() {return grupa;}
	public void setGrupa(Grupa grupa) {this.grupa = grupa;}
	
	public String getIme() {return ime;}
	public void setIme(String ime) {this.ime = ime;}
	
	public String getPrezime() {return prezime;}
	public void setPrezime(String prezime) {this.prezime = prezime;}
	
	public String getEmail() {return email;}
	public void setEmail(String email) {this.email = email;}
	
	public Byte getTippret() {return tippret;}
	public void setTippret(Byte tippret) {this.tippret = tippret;}
}