package Mladen.Teretana.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Pretplata
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_pretplate")
	private Integer idpretplate;
	
	@ManyToOne
	@JoinColumn(name="id_korisnika", nullable=false)
	private Korisnik korisnik;
	
	@Column(nullable=false)
	private LocalDate pocetak;
	
	@Column(nullable=false)
	private LocalDate kraj;
	
	public Integer getIdpretplate() {return idpretplate;}
		
	public Korisnik getKorisnik() {return korisnik;}
	public void setKorisnik(Korisnik korisnik) {this.korisnik = korisnik;}
	
	public LocalDate getPocetak() {return pocetak;}
	public void setPocetak(LocalDate pocetak) {this.pocetak = pocetak;}
	
	public LocalDate getKraj() {return kraj;}
	public void setKraj(LocalDate kraj) {this.kraj = kraj;}
}