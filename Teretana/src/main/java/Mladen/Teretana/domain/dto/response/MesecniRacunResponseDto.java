package Mladen.Teretana.domain.dto.response;

import java.util.List;

import Mladen.Teretana.domain.dto.DolazakDto;

public class MesecniRacunResponseDto
{
	private boolean uspesnost;
	private String errorMessage;
	private String ime;
	private String prezime;
	private double ukupanIznos;
	private List<DolazakDto> dolasci;
	
	public boolean isUspesnost() {return uspesnost;}
	public void setUspesnost(boolean uspesnost) {this.uspesnost = uspesnost;}
	
	public String getErrorMessage() {return errorMessage;}
	public void setErrorMessage(String errorMessage) {this.errorMessage = errorMessage;}
	
	public String getIme() {return ime;}
	public void setIme(String ime) {this.ime = ime;}
	
	public String getPrezime() {return prezime;}
	public void setPrezime(String prezime) {this.prezime = prezime;}
	
	public double getUkupanIznos() {return ukupanIznos;}
	public void setUkupanIznos(double ukupanIznos) {this.ukupanIznos = ukupanIznos;}
	
	public List<DolazakDto> getDolasci() {return dolasci;}
	public void setDolasci(List<DolazakDto> dolasci) {this.dolasci = dolasci;}
}