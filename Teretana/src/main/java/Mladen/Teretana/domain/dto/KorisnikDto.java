package Mladen.Teretana.domain.dto;

import java.util.List;

public class KorisnikDto
{
	private int id_korisnika;
	private String ime;
	private String prezime;
	private List<DolazakDto> dolasci;
	
	public int getId_korisnika() {return id_korisnika;}
	public void setId_korisnika(int id_korisnika) {this.id_korisnika = id_korisnika;}
	
	public String getIme() {return ime;}
	public void setIme(String ime) {this.ime = ime;}
	
	public String getPrezime() {return prezime;}
	public void setPrezime(String prezime) {this.prezime = prezime;}
	
	public List<DolazakDto> getDolasci() {return dolasci;}
	public void setDolasci(List<DolazakDto> dolasci) {this.dolasci = dolasci;}
}