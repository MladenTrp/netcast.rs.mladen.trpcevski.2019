package Mladen.Teretana.domain.dto.response;

import java.time.LocalDateTime;

import Mladen.Teretana.domain.Korisnik;
import Mladen.Teretana.domain.Operater;

public class DodajDolazakResponseDto
{
	private boolean uspesnost;
	private String errorMessage;
	private Operater operater;
	private Korisnik korisnik;
	private LocalDateTime vreme;
	private double iznos;
	
	public boolean isUspesnost() {return uspesnost;}
	public void setUspesnost(boolean uspesnost) {this.uspesnost = uspesnost;}
	
	public String getErrorMessage() {return errorMessage;}
	public void setErrorMessage(String errorMessage) {this.errorMessage = errorMessage;}
	
	public Operater getOperater() {return operater;}
	public void setOperater(Operater operater) {this.operater = operater;}
	
	public Korisnik getKorisnik() {return korisnik;}
	public void setKorisnik(Korisnik korisnik) {this.korisnik = korisnik;}
	
	public LocalDateTime getVreme() {return vreme;}
	public void setVreme(LocalDateTime vreme) {this.vreme = vreme;}
	
	public double getIznos() {return iznos;}
	public void setIznos(double iznos) {this.iznos = iznos;}
}