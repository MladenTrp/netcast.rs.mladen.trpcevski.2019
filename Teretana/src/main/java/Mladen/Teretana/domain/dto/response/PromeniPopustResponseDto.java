package Mladen.Teretana.domain.dto.response;

public class PromeniPopustResponseDto
{
	private boolean uspesnost;
	private String errorMessage;
	private byte stariPopust;
	private byte noviPopust;
	
	public boolean isUspesnost() {return uspesnost;}
	public void setUspesnost(boolean uspesnost) {this.uspesnost = uspesnost;}
	
	public String getErrorMessage() {return errorMessage;}
	public void setErrorMessage(String errorMessage) {this.errorMessage = errorMessage;}
	
	public byte getStariPopust() {return stariPopust;}
	public void setStariPopust(byte stariPopust) {this.stariPopust = stariPopust;}
	
	public byte getNoviPopust() {return noviPopust;}
	public void setNoviPopust(byte noviPopust) {this.noviPopust = noviPopust;}
}