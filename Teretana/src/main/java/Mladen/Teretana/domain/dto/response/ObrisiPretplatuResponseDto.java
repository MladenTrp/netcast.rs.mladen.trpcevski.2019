package Mladen.Teretana.domain.dto.response;

import Mladen.Teretana.domain.Pretplata;

public class ObrisiPretplatuResponseDto
{
	private boolean uspesnost;
	private String errorMessage;
	private Pretplata pretplata;
	
	public boolean isUspesnost() {return uspesnost;}
	public void setUspesnost(boolean uspesnost) {this.uspesnost = uspesnost;}
	
	public String getErrorMessage() {return errorMessage;}
	public void setErrorMessage(String errorMessage) {this.errorMessage = errorMessage;}
	
	public Pretplata getPretplata() {return pretplata;}
	public void setPretplata(Pretplata pretplata) {this.pretplata = pretplata;}
}