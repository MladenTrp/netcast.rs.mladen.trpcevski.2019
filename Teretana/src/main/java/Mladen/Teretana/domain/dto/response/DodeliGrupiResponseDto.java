package Mladen.Teretana.domain.dto.response;

import Mladen.Teretana.domain.Korisnik;

public class DodeliGrupiResponseDto
{
	private boolean uspesnost;
	private String errorMessage;
	private Korisnik korisnik;
	
	public boolean isUspesnost() {return uspesnost;}
	public void setUspesnost(boolean uspesnost) {this.uspesnost = uspesnost;}
	
	public String getErrorMessage() {return errorMessage;}
	public void setErrorMessage(String errorMessage) {this.errorMessage = errorMessage;}
	
	public Korisnik getKorisnik() {return korisnik;}
	public void setKorisnik(Korisnik korisnik) {this.korisnik = korisnik;}
}