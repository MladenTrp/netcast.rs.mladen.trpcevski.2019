package Mladen.Teretana.domain.dto.response;

public class RegistrujOperateraResponseDto
{
	private boolean uspesnost;
	private String errorMessage;
	
	public boolean isUspesnost() {return uspesnost;}
	public void setUspesnost(boolean uspesnost) {this.uspesnost = uspesnost;}
	
	public String getErrorMessage() {return errorMessage;}
	public void setErrorMessage(String errorMessage) {this.errorMessage = errorMessage;}
}