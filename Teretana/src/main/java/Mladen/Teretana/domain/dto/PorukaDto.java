package Mladen.Teretana.domain.dto;

import java.util.List;

public class PorukaDto
{
	private int idKorisnika;
	private String ime;
	private String prezime;
	private String email;
	private double pretplata;
	private List<DolazakDto2> dolasci;
	private double ukupanIznos;
	
	public int getIdKorisnika() {return idKorisnika;}
	public void setIdKorisnika(int idKorisnika) {this.idKorisnika = idKorisnika;}
	
	public String getIme() {return ime;}
	public void setIme(String ime) {this.ime = ime;}
	
	public String getPrezime() {return prezime;}
	public void setPrezime(String prezime) {this.prezime = prezime;}
	
	public String getEmail() {return email;}
	public void setEmail(String email) {this.email = email;}
	
	public double getPretplata() {return pretplata;}
	public void setPretplata(double pretplata) {this.pretplata = pretplata;}
	
	public List<DolazakDto2> getDolasci() {return dolasci;}
	public void setDolasci(List<DolazakDto2> dolasci) {this.dolasci = dolasci;}
	
	public double getUkupanIznos() {return ukupanIznos;}
	public void setUkupanIznos(double ukupanIznos) {this.ukupanIznos = ukupanIznos;}
}