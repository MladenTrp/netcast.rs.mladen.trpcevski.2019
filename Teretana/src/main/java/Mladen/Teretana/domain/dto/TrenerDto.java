package Mladen.Teretana.domain.dto;

import java.util.List;

public class TrenerDto
{
	private Integer id_trenera;
	private String ime;
	private String prezime;
	private Double cena;
	private List<VezbanjeDto> treninzi;
	
	public Integer getId_trenera() {return id_trenera;}
	public void setId_trenera(Integer id_trenera) {this.id_trenera = id_trenera;}
	
	public String getIme() {return ime;}
	public void setIme(String ime) {this.ime = ime;}
	
	public String getPrezime() {return prezime;}
	public void setPrezime(String prezime) {this.prezime = prezime;}
	
	public Double getCena() {return cena;}
	public void setCena(Double cena) {this.cena = cena;}
	
	public List<VezbanjeDto> getTreninzi() {return treninzi;}
	public void setTreninzi(List<VezbanjeDto> treninzi) {this.treninzi = treninzi;}
}