package Mladen.Teretana.domain.dto;

import java.time.LocalDate;

public class DolazakDto2
{
	private LocalDate datum;
	private double iznos;
	
	public LocalDate getDatum() {return datum;}
	public void setDatum(LocalDate datum) {this.datum = datum;}
	
	public double getIznos() {return iznos;}
	public void setIznos(double iznos) {this.iznos = iznos;}
}