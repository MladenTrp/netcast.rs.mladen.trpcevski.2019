package Mladen.Teretana.domain.dto.response;

public class DodajKorisnikaResponseDto
{
	private String ime;
	private String prezime;
	private String email;
	
	public String getIme() {return ime;}
	public void setIme(String ime) {this.ime = ime;}
	
	public String getPrezime() {return prezime;}
	public void setPrezime(String prezime) {this.prezime = prezime;}
	
	public String getEmail() {return email;}
	public void setEmail(String email) {this.email = email;}
}