package Mladen.Teretana.domain.dto;

import java.time.LocalDate;

public class PretplataDto
{
	private String ime;
	private String prezime;
	private LocalDate pocetak;
	private LocalDate kraj;
	
	public String getIme() {return ime;}
	public void setIme(String ime) {this.ime = ime;}
	
	public String getPrezime() {return prezime;}
	public void setPrezime(String prezime) {this.prezime = prezime;}
	
	public LocalDate getPocetak() {return pocetak;}
	public void setPocetak(LocalDate pocetak) {this.pocetak = pocetak;}
	
	public LocalDate getKraj() {return kraj;}
	public void setKraj(LocalDate kraj) {this.kraj = kraj;}
}