package Mladen.Teretana.domain.dto;

import java.time.LocalDateTime;

public class DolazakDto
{
	private LocalDateTime dolazak;
	private LocalDateTime odlazak;
	private int id_trenera;
	private double iznos;
	
	public LocalDateTime getDolazak() {return dolazak;}
	public void setDolazak(LocalDateTime dolazak) {this.dolazak = dolazak;}
	
	public LocalDateTime getOdlazak() {return odlazak;}
	public void setOdlazak(LocalDateTime odlazak) {this.odlazak = odlazak;}
	
	public int getId_trenera() {return id_trenera;}
	public void setId_trenera(int id_trenera) {this.id_trenera = id_trenera;}
	
	public double getIznos() {return iznos;}
	public void setIznos(double iznos) {this.iznos = iznos;}
}