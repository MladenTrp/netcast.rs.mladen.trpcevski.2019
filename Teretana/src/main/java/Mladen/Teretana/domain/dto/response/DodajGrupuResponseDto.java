package Mladen.Teretana.domain.dto.response;

import Mladen.Teretana.domain.Grupa;

public class DodajGrupuResponseDto
{
	private boolean uspesnost;
	private String errorMessage;
	private Grupa grupa;
	
	public boolean isUspesnost() {return uspesnost;}
	public void setUspesnost(boolean uspesnost) {this.uspesnost = uspesnost;}
	
	public String getErrorMessage() {return errorMessage;}
	public void setErrorMessage(String errorMessage) {this.errorMessage = errorMessage;}
	
	public Grupa getGrupa() {return grupa;}
	public void setGrupa(Grupa grupa) {this.grupa = grupa;}
}