package Mladen.Teretana.domain.dto.response;

import java.util.List;

import Mladen.Teretana.domain.dto.DolazakDto;

public class ListajDolaskeZaIdResponseDto
{
	private boolean uspesnost;
	private String errorMessage;
	private List<DolazakDto> dolasci;
	
	public boolean isUspesnost() {return uspesnost;}
	public void setUspesnost(boolean uspesnost) {this.uspesnost = uspesnost;}
	
	public String getErrorMessage() {return errorMessage;}
	public void setErrorMessage(String errorMessage) {this.errorMessage = errorMessage;}
	
	public List<DolazakDto> getDolasci() {return dolasci;}
	public void setDolasci(List<DolazakDto> dolasci) {this.dolasci = dolasci;}
}