package Mladen.Teretana.domain.dto.response;

import Mladen.Teretana.domain.Korisnik;
import Mladen.Teretana.domain.Trener;

public class DodajTreningResponseDto
{
	private boolean uspesnost;
	private String errorMessage;
	private Korisnik korisnik;
	private Trener trener;
	private double noviIznos;
	
	public boolean isUspesnost() {return uspesnost;}
	public void setUspesnost(boolean uspesnost) {this.uspesnost = uspesnost;}
	
	public String getErrorMessage() {return errorMessage;}
	public void setErrorMessage(String errorMessage) {this.errorMessage = errorMessage;}
	
	public Korisnik getKorisnik() {return korisnik;}
	public void setKorisnik(Korisnik korisnik) {this.korisnik = korisnik;}
	
	public Trener getTrener() {return trener;}
	public void setTrener(Trener trener) {this.trener = trener;}
	
	public double getNoviIznos() {return noviIznos;}
	public void setNoviIznos(double noviIznos) {this.noviIznos = noviIznos;}
}