package Mladen.Teretana.domain.dto.response;

import java.time.LocalDate;

import Mladen.Teretana.domain.Korisnik;

public class PretplatiResponseDto
{
	private boolean uspesnost;
	private String errorMessage;
	private Korisnik korisnik;
	private LocalDate pocetak;
	private LocalDate kraj;
	
	public boolean isUspesnost() {return uspesnost;}
	public void setUspesnost(boolean uspesnost) {this.uspesnost = uspesnost;}
	
	public String getErrorMessage() {return errorMessage;}
	public void setErrorMessage(String errorMessage) {this.errorMessage = errorMessage;}
	
	public Korisnik getKorisnik() {return korisnik;}
	public void setKorisnik(Korisnik korisnik) {this.korisnik = korisnik;}
	
	public LocalDate getPocetak() {return pocetak;}
	public void setPocetak(LocalDate pocetak) {this.pocetak = pocetak;}
	
	public LocalDate getKraj() {return kraj;}
	public void setKraj(LocalDate kraj) {this.kraj = kraj;}
}