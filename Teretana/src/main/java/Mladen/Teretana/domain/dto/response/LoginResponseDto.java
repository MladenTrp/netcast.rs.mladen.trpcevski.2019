package Mladen.Teretana.domain.dto.response;

public class LoginResponseDto
{
	private boolean uspesnost;
	private String errorMessage;
	private String token;
	
	public boolean isUspesnost() {return uspesnost;}
	public void setUspesnost(boolean uspesnost) {this.uspesnost = uspesnost;}
	
	public String getErrorMessage() {return errorMessage;}
	public void setErrorMessage(String errorMessage) {this.errorMessage = errorMessage;}
	
	public String getToken() {return token;}
	public void setToken(String token) {this.token = token;}
}