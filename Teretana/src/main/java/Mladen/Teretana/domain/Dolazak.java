package Mladen.Teretana.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Dolazak
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_dolaska")
	private Integer iddolaska;
	
	@ManyToOne
	@JoinColumn(name="id_operatera", nullable=false)
	private Operater operater;
	
	@ManyToOne
	@JoinColumn(name="id_korisnika", nullable=false)
	private Korisnik korisnik;
	
	@ManyToOne
	@JoinColumn(name="id_trenera")
	private Trener trener;
	
	@Column(nullable=false)
	private LocalDateTime dolazak;
	private LocalDateTime odlazak;
	
	@Column(nullable=false)
	private Double iznos;
	
	public Integer getIddolaska() {return iddolaska;}
		
	public Operater getOperater() {return operater;}
	public void setOperater(Operater operater) {this.operater = operater;}
	
	public Korisnik getKorisnik() {return korisnik;}
	public void setKorisnik(Korisnik korisnik) {this.korisnik = korisnik;}
	
	public Trener getTrener() {return trener;}
	public void setTrener(Trener trener) {this.trener = trener;}
	
	public LocalDateTime getDolazak() {return dolazak;}
	public void setDolazak(LocalDateTime dolazak) {this.dolazak = dolazak;}
	
	public LocalDateTime getOdlazak() {return odlazak;}
	public void setOdlazak(LocalDateTime odlazak) {this.odlazak = odlazak;}
	
	public Double getIznos() {return iznos;}
	public void setIznos(Double iznos) {this.iznos = iznos;}
}