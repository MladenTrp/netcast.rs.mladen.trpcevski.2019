package Mladen.Teretana.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Grupa
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_grupe")
	private Integer idgrupe;
	
	@JsonBackReference
	@OneToMany(mappedBy="grupa")
	private List<Korisnik> korisnici;
	
	@Column(nullable=false)
	private String naziv;
	
	@Column(nullable=false)
	private Byte popust;

	public Integer getIdgrupe() {return idgrupe;}

	public List<Korisnik> getKorisnici() {return korisnici;}
	public void setKorisnici(List<Korisnik> korisnici) {this.korisnici = korisnici;}

	public String getNaziv() {return naziv;}
	public void setNaziv(String naziv) {this.naziv = naziv;}

	public Byte getPopust() {return popust;}
	public void setPopust(Byte popust) {this.popust = popust;}
}