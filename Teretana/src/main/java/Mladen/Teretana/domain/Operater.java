package Mladen.Teretana.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Operater
{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_operatera")
	private Integer idoperatera;
	
	@JsonBackReference
	@OneToMany(mappedBy="operater")
	private List<Dolazak> dolasci;
	
	@Column(nullable=false)
	private String ime;
	
	@Column(nullable=false)
	private String prezime;
	
	@Column(nullable=false)
	private String email;
	
	@Column(nullable=false)
	private String lozinka;

	public Integer getIdoperatera() {return idoperatera;}

	public List<Dolazak> getDolasci() {return dolasci;}
	public void setDolasci(List<Dolazak> dolasci) {this.dolasci = dolasci;}

	public String getIme() {return ime;}
	public void setIme(String ime) {this.ime = ime;}

	public String getPrezime() {return prezime;}
	public void setPrezime(String prezime) {this.prezime = prezime;}

	public String getEmail() {return email;}
	public void setEmail(String email) {this.email = email;}

	public String getLozinka() {return lozinka;}
	public void setLozinka(String lozinka) {this.lozinka = lozinka;}
}