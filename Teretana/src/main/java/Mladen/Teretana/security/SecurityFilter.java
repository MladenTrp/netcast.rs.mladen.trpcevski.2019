package Mladen.Teretana.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SecurityFilter implements Filter
{
	private final String HEADER = "token";
	// private final String TOKEN_PREFIX = "Bearer ";
	
	@Autowired
	private TokenService tokenServis;
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException
	{
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		
		if ((httpRequest.getRequestURI().contains("/login"))||(httpRequest.getRequestURI().contains("/jsondoc")))
		{
			chain.doFilter(httpRequest, httpResponse);
			return;
		}
		String token = httpRequest.getHeader(HEADER);
		if (token==null)
		{
			httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}
		String email = tokenServis.validirajToken(token);
		if (email==null)
		{
			httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}
		request.setAttribute("email", email);
		chain.doFilter(request, response);
	}
}