package Mladen.Teretana.api;

import java.util.List;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.annotation.ApiQueryParam;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.jsondoc.core.pojo.ApiVerb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import Mladen.Teretana.domain.Grupa;
import Mladen.Teretana.domain.dto.response.DodeliGrupiResponseDto;
import Mladen.Teretana.domain.dto.response.IzbaciIzGrupeResponseDto;
import Mladen.Teretana.domain.dto.response.DodajGrupuResponseDto;
import Mladen.Teretana.domain.dto.response.ObrisiGrupuResponseDto;
import Mladen.Teretana.domain.dto.response.PromeniPopustResponseDto;
import Mladen.Teretana.service.GrupaService;

@Api(name="Grupa", description="Putanje za upite nad posebnim grupama korisnika	.")
@RestController
@RequestMapping("/grupe")
public class GrupaController
{
	@Autowired
	private GrupaService servis;
	
	@ApiMethod(path="/grupe/add", summary="stvaranje nove grupe korisnika", description="Dodaje se nova grupa korisnika. Kao parametri se prosledjuju naziv grupe i celobrojni popust kao broj procenata (X = X%).", verb=ApiVerb.POST)
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public @ApiResponseObject @ResponseBody DodajGrupuResponseDto dodajGrupu(@ApiQueryParam(name="naziv", description="naziv grupe") @RequestParam String naziv, @ApiQueryParam(name="popust", description="procenat popusta, izrazen kao ceo broj") @RequestParam Byte popust)
	{
		return servis.dodajGrupuS(naziv, popust);
	}
	
	@ApiMethod(path="/grupe/all", summary="listanje svih grupa", description="Listaju se sve postojece grupe.", verb=ApiVerb.GET)
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ApiResponseObject @ResponseBody List<Grupa> listajGrupe()
	{
		return servis.listajGrupeS();
	}
	
	@ApiMethod(path="/grupe/update", summary="promena popusta za izabranu grupu", description="Menja se visina popusta za odabranu grupu korisnika. Kao parametri se prosledjuju ID grupe i novi celobrojni popust (X = X%).", verb=ApiVerb.POST)
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ApiResponseObject @ResponseBody PromeniPopustResponseDto promeniPopust(@ApiQueryParam(name="id", description="ID grupe za koju se menja popust") @RequestParam Integer id, @ApiQueryParam(name="popust", description="novi procenat popusta, izrazen kao ceo broj") @RequestParam Byte popust)
	{
		return servis.promeniPopustS(id, popust);
	}
	
	@ApiMethod(path="/grupe/delete/{id}", summary="brisanje izabrane grupe", description="Brise se grupa koja ima uneti ID.", verb=ApiVerb.DELETE)
	@RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE)
	public @ApiResponseObject @ResponseBody ObrisiGrupuResponseDto obrisiGrupu(@ApiPathParam(name="id", description="ID grupe koju treba obrisati") @PathVariable Integer id)
	{
		return servis.obrisiGrupuS(id);
	}
	
	@ApiMethod(path="/grupe/{idg}/add/{idk}", summary="dodeljivanje grupe korisniku", description="Izabranoj grupi se dodeljuje izabrani korisnik.", verb=ApiVerb.POST)
	@RequestMapping(value="/{idg}/add/{idk}", method=RequestMethod.POST)
	public @ApiResponseObject @ResponseBody DodeliGrupiResponseDto dodeliGrupi(@ApiPathParam(name="idg", description="ID grupe kojoj se dodeljuje korisnik") @PathVariable Integer idg, @ApiPathParam(name="idk", description="ID korisnika koji se dodaje u grupu") @PathVariable Integer idk)
	{
		return servis.dodeliGrupiS(idg, idk);
	}
	
	@ApiMethod(path="/grupe/{idg}/remove/{idk}", summary="uklanjanje korisnika iz grupe", description="Naznaceni korisnik se uklanja iz naznacene grupe.", verb=ApiVerb.POST)
	@RequestMapping(value="/{idg}/remove/{idk}", method=RequestMethod.POST)
	public @ApiResponseObject @ResponseBody IzbaciIzGrupeResponseDto izbaciIzGrupe(@ApiPathParam(name="idg", description="ID grupe iz koje se uklanja korisnik") @PathVariable Integer idg, @ApiPathParam(name="idk", description="ID korisnika koji se uklanja iz grupe") @PathVariable Integer idk)
	{
		return servis.izbaciIzGrupeS(idg, idk);
	}
}