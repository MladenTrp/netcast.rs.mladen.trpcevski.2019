package Mladen.Teretana.api;

import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.annotation.ApiQueryParam;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.jsondoc.core.pojo.ApiVerb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import Mladen.Teretana.domain.Dolazak;
import Mladen.Teretana.domain.dto.KorisnikDto;
import Mladen.Teretana.domain.dto.response.DodajDolazakResponseDto;
import Mladen.Teretana.domain.dto.response.DodajOdlazakResponseDto;
import Mladen.Teretana.domain.dto.response.DodajTreningResponseDto;
import Mladen.Teretana.domain.dto.response.MesecniRacunResponseDto;
import Mladen.Teretana.service.DolazakService;

@Api(name="Dolazak", description="Putanje za upite nad dolascima.")
@RestController
@RequestMapping("/dolasci")
public class DolazakController 
{
	@Autowired
	private DolazakService servis;
	
	@ApiMethod(path="/dolasci/add/{idk}", summary="stvaranje novog dolaska", description="Evidentira se dolazak korisnika u sadasnjem trenutku. Naplacuje se jednokratni iznos ukoliko korisnik nema pretplatu. Vreme odlaska ostaje nepopunjeno, jer se odlazak belezi u odvojenoj metodi.", verb=ApiVerb.POST)
	@RequestMapping(value="/add/{idk}", method=RequestMethod.POST)
	public @ApiResponseObject @ResponseBody DodajDolazakResponseDto dodajDolazak(@ApiPathParam(name="idk", description="ID korisnika koji je dosao") @PathVariable Integer idk, HttpServletRequest request)
	{
		String email = (String) request.getAttribute("email");
		return servis.dodajDolazakS(idk, email);
	}
	
	@ApiMethod(path="/dolasci/add/{idk}/{idt}", summary="evidentiranje treninga", description="Evidentira se trening unutar nezavrsenog dolaska. Iznosu za naplatu se dodaje cena trenera. Jednom dolasku mose se dodati najvise jedan trening.", verb=ApiVerb.POST)
	@RequestMapping(value="/add/{idk}/{idt}", method=RequestMethod.POST)
	public @ApiResponseObject @ResponseBody DodajTreningResponseDto dodajTrening(@ApiPathParam(name="idk", description="ID korisnika koji koristi usluge trenera") @PathVariable("idk") Integer idk, @ApiPathParam(name="idt", description="ID trenera cije usluge se koriste") @PathVariable("idt") Integer idt)
	{
		return servis.dodajTreningS(idk, idt);
	}
	
	@ApiMethod(path="/dolasci/kraj/{idk}", summary="evidentiranje odlaska", description="Evidentira se da je korisnik otisao u sadasnjem trenutku.", verb=ApiVerb.POST)
	@RequestMapping(value="/kraj/{idk}", method=RequestMethod.POST)
	public @ApiResponseObject @ResponseBody DodajOdlazakResponseDto dodajOdlazak(@ApiPathParam(name="idk", description="ID korisnika koji je otisao") @PathVariable Integer idk)
	{
		return servis.dodajOdlazakS(idk);
	}
	
	@ApiMethod(path="/dolasci/all", summary="listanje svih dolazaka", description="Listaju se svi dolasci iz baze.", verb=ApiVerb.GET)
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ApiResponseObject List<Dolazak> listajSveDolaske()
	{
		return servis.listajSveDolaskeS();
	}
	
	@ApiMethod(path="/dolasci/all/interval", summary="listanje svih dolazaka unutar zadatog intervala", description="Listaju se svi dolasci unutar zadatog intervala. Vreme se unosi u formatu yyyy-MM-dd HH:mm:ss .", verb=ApiVerb.GET)
	@RequestMapping(value="/all/interval", method=RequestMethod.GET)
	public @ApiResponseObject List<Dolazak> listajIzmedju(@ApiQueryParam(name="vreme1", description="pocetak intervala", format="yyyy-MM-dd HH:mm:ss") @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime vreme1, @ApiQueryParam(name="vreme2", description="kraj intervala", format="yyyy-MM-dd HH:mm:ss") @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime vreme2)
	{
		return servis.listajIzmedjuS(vreme1, vreme2);
	}
	
	@ApiMethod(path="/dolasci/allkor/interval", summary="listanje svih korisnika koji su dolazili u zadatom intervalu", description="Listaju se svi korisnici koji su dolazili u datom intervalu, kao i odgovarajuci dolasci. Vreme se unosi u formatu yyyy-MM-dd HH:mm:ss .", verb=ApiVerb.GET)
	@RequestMapping(value="/allkor/interval", method=RequestMethod.GET)
	public @ApiResponseObject List<KorisnikDto> listajKorIzmedju(@ApiQueryParam(name="vreme1", description="pocetak intervala", format="yyyy-MM-dd HH:mm:ss") @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime vreme1, @ApiQueryParam(name="vreme2", description="kraj intervala", format="yyyy-MM-dd HH:mm:ss") @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime vreme2)
	{
		return servis.listajKorIzmedjuS(vreme1, vreme2);
	}
	
	@ApiMethod(path="/dolasci/racun/{idk}", summary="prikaz mesecnog racuna za unetog korisnika", description="Prikazuje se ukupan racun korisnika za tekuci mesec, zajedno sa dolascima i njima odgovarajucim placenim iznosima.", verb=ApiVerb.GET)
	@RequestMapping(value="/racun/{idk}", method=RequestMethod.GET)
	public @ApiResponseObject MesecniRacunResponseDto mesecniRacun(@ApiPathParam(name="idk", description="ID korisnika ciji se mesecni racun prikazuje") @PathVariable Integer idk)
	{
		return servis.mesecniRacunS(idk);
	}
	
	@ApiMethod(path="/dolasci/prihodi", summary="prikaz ukupnih prihoda teretane", description="Prikazuje se iznos ukupno ostvarenog prihoda od pocetka poslovanja.", verb=ApiVerb.GET)
	@RequestMapping(value="/prihodi", method=RequestMethod.GET)
	public String prihodiSvi()
	{
		return servis.prihodiSviS();
	}
}