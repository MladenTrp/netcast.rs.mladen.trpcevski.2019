package Mladen.Teretana.api;

import java.util.List;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.annotation.ApiQueryParam;
import org.jsondoc.core.pojo.ApiVerb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import Mladen.Teretana.domain.Trener;
import Mladen.Teretana.domain.dto.TrenerDto;
import Mladen.Teretana.domain.dto.response.DodajTreneraResponseDto;
import Mladen.Teretana.domain.dto.response.NadjiIdTreneraResponseDto;
import Mladen.Teretana.service.TrenerService;

@Api(name="Trener", description="Putanje za upite nad trenerima.")
@RestController
@RequestMapping("/treneri")
public class TrenerController 
{
	@Autowired
	private TrenerService servis;
	
	@ApiMethod(path="/treneri/add", summary="dodavanje novog trenera", description="Dodaje se novi trener.", verb=ApiVerb.POST)
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public @ResponseBody DodajTreneraResponseDto dodajTrenera(@ApiQueryParam(name="ime", description="ime trenera") @RequestParam String ime, @ApiQueryParam(name="prezime", description="prezime trenera") @RequestParam String prezime, @ApiQueryParam(name="cena", description="cena usluge trenera (moze biti decimalna)") @RequestParam Double cena)
	{
		return servis.dodajTreneraS(ime, prezime, cena);
	}
	
	@ApiMethod(path="/treneri/all", summary="listanje svih trenera (bez odrzanih treninga)", description="Listaju se svi treneri (bez informacija o odrzanim treninzima).", verb=ApiVerb.GET)
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody List<Trener> listajTrenere()
	{
		return servis.listajTrenereS();
	}
	
	@ApiMethod(path="/treneri/treninzi", summary="listanje svih trenera (sa odrzanim treninzima)", description="Listaju se svi treneri, sa podacima o odrzanim treninzima.", verb=ApiVerb.GET)
	@RequestMapping(value="/treninzi", method=RequestMethod.GET)
	public @ResponseBody List<TrenerDto> listajTreninge()
	{
		return servis.listajTreningeS();
	}
	
	@ApiMethod(path="/treneri/{id}", summary="trazenje trenera po ID-u", description="Prikazuje se trener koji ime uneti ID, ukoliko postoji.", verb=ApiVerb.GET)
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public @ResponseBody NadjiIdTreneraResponseDto nadjiIdTrenera(@ApiPathParam(name="id", description="ID trenera") @PathVariable Integer id)
	{
		return servis.nadjiIdTreneraS(id);
	}
}