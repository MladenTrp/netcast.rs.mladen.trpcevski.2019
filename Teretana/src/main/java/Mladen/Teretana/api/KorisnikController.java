package Mladen.Teretana.api;

import java.util.List;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.annotation.ApiQueryParam;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.jsondoc.core.pojo.ApiVerb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import Mladen.Teretana.domain.Korisnik;
import Mladen.Teretana.domain.dto.KorisnikDto;
import Mladen.Teretana.domain.dto.response.DodajKorisnikaResponseDto;
import Mladen.Teretana.domain.dto.response.ListajDolaskeZaIdResponseDto;
import Mladen.Teretana.domain.dto.response.NadjiIdKorisnikaResponseDto;
import Mladen.Teretana.service.KorisnikService;

@Api(name="Korisnik", description="Putanje za upite nad korisnicima.")
@RestController
@RequestMapping("/korisnici")
public class KorisnikController 
{
	@Autowired
	private KorisnikService servis;
	
	@ApiMethod(path="/korisnici/add", summary="dodavanje novog korisnika", description="Dodaje se novi korisnik.", verb=ApiVerb.POST)
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public @ApiResponseObject @ResponseBody DodajKorisnikaResponseDto dodajKorisnika(@ApiQueryParam(name="ime", description="ime novog korisnika") @RequestParam String ime, @ApiQueryParam(name="prezime", description="prezime novog korisnika") @RequestParam String prezime, @ApiQueryParam(name="e-mail", description="e-mail novog korisnika") @RequestParam String email)
	{
		return servis.dodajKorisnikaS(ime, prezime, email);
	}
	
	@ApiMethod(path="/korisnici/all", summary="listanje svih korisnika", description="Listaju se svi korisnici.", verb=ApiVerb.GET)
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ApiResponseObject @ResponseBody List<Korisnik> listajKorisnike()
	{
		return servis.listajKorisnikeS();
	}
	
	@ApiMethod(path="/korisnici/all/dolasci", summary="listanje svih korisnika (preglednije)", description="Listaju se svi korisnici (bez e-mail adrese) i njihovi dolasci (za trenere se prikazuje samo njihov ID).", verb=ApiVerb.GET)
	@RequestMapping(value="/all/dolasci", method=RequestMethod.GET)
	public @ApiResponseObject @ResponseBody List<KorisnikDto> listajKorisnikeDto()
	{
		return servis.listajKorisnikeDtoS();
	}
	
	@ApiMethod(path="/korisnici/{id}", summary="trazenje korisnika po ID-u", description="Prikazuje se korisnik za uneti ID, ukoliko postoji.", verb=ApiVerb.GET)
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public @ApiResponseObject @ResponseBody NadjiIdKorisnikaResponseDto nadjiIdKorisnika(@ApiPathParam(name="id", description="ID korisnika") @PathVariable Integer id)
	{
		return servis.nadjiIdKorisnikaS(id);
	}
	
	@ApiMethod(path="/korisnici/", summary="trazenje korisnika po prezimenu", description="Prikazuju se korisnici koji imaju uneto prezime.", verb=ApiVerb.GET)
	@RequestMapping(value="/", method=RequestMethod.GET) // Ne izbacuje poruku ako ne postoji prezime, nego vraca praznu listu
	public @ApiResponseObject @ResponseBody List<KorisnikDto> nadjiPrezime(@ApiQueryParam(name="prezime", description="prezime korisnika") @RequestParam String prezime)
	{
		return servis.nadjiPrezimeS(prezime);
	}
	
	@ApiMethod(path="/korisnici/{id}/dolasci", summary="listanje svih dolazaka odabranog korisnika", description="Listaju se svi dolasci korisnika koji ima uneti ID, ukoliko postoji.", verb=ApiVerb.GET)
	@RequestMapping(value="/{id}/dolasci", method=RequestMethod.GET)
	public @ApiResponseObject @ResponseBody ListajDolaskeZaIdResponseDto listajDolaskeZaId(@ApiPathParam(name="id", description="id korisnika") @PathVariable Integer id)
	{
		return servis.listajDolaskeZaIdS(id);
	}
}