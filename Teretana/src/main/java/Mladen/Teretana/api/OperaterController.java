package Mladen.Teretana.api;

import java.util.List;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.jsondoc.core.pojo.ApiVerb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import Mladen.Teretana.domain.dto.OperaterDto;
import Mladen.Teretana.domain.dto.request.LoginRequestDto;
import Mladen.Teretana.domain.dto.request.OperaterRequestDto;
import Mladen.Teretana.domain.dto.response.LoginResponseDto;
import Mladen.Teretana.domain.dto.response.RegistrujOperateraResponseDto;
import Mladen.Teretana.service.OperaterService;

@Api(name="Operater", description="Putanje za upite nad operaterima.")
@RestController
@RequestMapping("/operateri")
public class OperaterController
{
	@Autowired
	private OperaterService servis;
	
	@ApiMethod(path="/operateri/signup", summary="registrovanje novog operatera", description="Registruje se novi operater. Zbog osetljivosti, podaci se unose preko posebnog DTO-a. Neophodno je priloziti i vazeci token u header-u (tj. samo postojeci operater moze registrovati novog).", verb=ApiVerb.POST)
	@RequestMapping(value="/signup", method=RequestMethod.POST)
	public @ApiResponseObject @ResponseBody RegistrujOperateraResponseDto registrujOperatera(@RequestBody OperaterRequestDto operaterDto)
	{
		return servis.registrujOperateraS(operaterDto);
	}
	
	@ApiMethod(path="/operateri/login", summary="logovanje operatera", description="Postojeci operater se loguje prosledjivanjem e-maila i lozinke putem DTO-a. Ukoliko je logovanje uspesno, metoda vraca token koji vazi 24 sata i koji treba priloziti prilikom poziva bilo kojeg drugog API-ja.", verb=ApiVerb.POST)
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public @ApiResponseObject @ResponseBody LoginResponseDto login(@RequestBody LoginRequestDto loginDto)
	{
		return servis.loginS(loginDto);
	}
	
	@ApiMethod(path="/operateri/all", summary="listanje operatera", description="Listaju se svi operateri, kao i termini koje su uneli.", verb=ApiVerb.GET)
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ApiResponseObject @ResponseBody List<OperaterDto> listajOperatere()
	{
		return servis.listajOperatereS();
	}
}