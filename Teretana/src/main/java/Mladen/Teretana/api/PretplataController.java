package Mladen.Teretana.api;

import java.util.List;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.pojo.ApiVerb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import Mladen.Teretana.domain.Pretplata;
import Mladen.Teretana.domain.dto.PretplataDto;
import Mladen.Teretana.domain.dto.response.ObrisiPretplatuResponseDto;
import Mladen.Teretana.domain.dto.response.PretplatiResponseDto;
import Mladen.Teretana.service.PretplataService;

@Api(name="Pretplata", description="Putanje za upite nad pretplatama.")
@RestController
@RequestMapping("/pretplate")
public class PretplataController 
{
	@Autowired
	private PretplataService servis;
	
	@ApiMethod(path="/pretplate/add/{idk}", summary="evidentiranje nove pretplate", description="Registruje se nova pretplata za unetog korisnika.", verb=ApiVerb.POST)
	@RequestMapping(value="/add/{idk}", method=RequestMethod.POST)
	public @ResponseBody PretplatiResponseDto pretplati(@ApiPathParam(name="idk", description="ID korisnika za kojeg se unosi pretplata") @PathVariable Integer idk)
	{
		return servis.pretplatiS(idk);
	}
	
	@ApiMethod(path="/pretplate/all", summary="listanje svih pretplata", description="Listaju se sve tekuce i arhivirane pretplate.", verb=ApiVerb.GET)
	@RequestMapping(value="/all", method=RequestMethod.GET) // Opadajucim redosledom po datumu isticanja pretplate
	public @ResponseBody List<Pretplata> listajSve()
	{
		return servis.listajSveS();
	}
	
	@ApiMethod(path="/pretplate/all/update", summary="azuriranje pretplata", description="Pretplate se azuriraju tj. koriguje se status pretplate za korisnike cija pretplata je istekla od prethodnog azuriranja.", verb=ApiVerb.POST)
	@RequestMapping(value="/all/update", method=RequestMethod.POST)
	@PostMapping("/all/update")
	public @ResponseBody List<PretplataDto> azurirajPretplate()
	{
		return servis.azurirajPretplateS();
	}
	
	@ApiMethod(path="/pretplate/delete/{idk}", summary="brisanje pretplate", description="Brise se uneta pretplata, pod uslovom da nije istekla (arhivirana).", verb=ApiVerb.POST)
	@RequestMapping(value="/delete/{idk}", method=RequestMethod.POST)
	@PostMapping("/delete/{idk}")
	public @ResponseBody ObrisiPretplatuResponseDto obrisiPretplatu(@ApiPathParam(name="idk", description="ID korisnika kojem se brise tekuca pretplata") @PathVariable Integer idk)
	{
		return servis.obrisiPretplatuS(idk);
	}
}