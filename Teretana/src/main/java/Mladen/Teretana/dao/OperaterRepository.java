package Mladen.Teretana.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import Mladen.Teretana.domain.Operater;

@Repository
public interface OperaterRepository extends JpaRepository<Operater, Integer>
{
	public List<Operater> findByEmail(String email);
	public List<Operater> findByEmailAndLozinka(String email, String lozinka);
}