package Mladen.Teretana.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import Mladen.Teretana.domain.Grupa;

@Repository
public interface GrupaRepository extends JpaRepository<Grupa, Integer>
{
	public List<Grupa> findByNaziv(String naziv);
}