package Mladen.Teretana.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import Mladen.Teretana.domain.Trener;

@Repository
public interface TrenerRepository extends JpaRepository<Trener, Integer>
{

}