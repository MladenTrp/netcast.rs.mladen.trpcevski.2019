package Mladen.Teretana.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import Mladen.Teretana.domain.Korisnik;

@Repository
public interface KorisnikRepository extends JpaRepository<Korisnik, Integer>
{
	public List<Korisnik> findByPrezime(String prezime);
}